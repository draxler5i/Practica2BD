USE Practica2 ;

INSERT INTO Persona (persona_id, nombre, apellido, genero, fechaNacimiento)
VALUES	(11, 'Matilda', 'Almeida',	'F', '1990-02-14'),
		(12, 'Pedro', 'Blanco',	'M', '1970-02-14'),
		(13, 'Jorge', 'Galindo', 'M', '1995-10-21');