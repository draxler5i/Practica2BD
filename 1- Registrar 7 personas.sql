USE Practica2 ;

INSERT INTO Persona 
VALUES	(1, 'Carlos', 'Almeida',	'M', '1990-02-14', 'Brasil'),
		(2, 'Marco', 'Zarate',	'M', '1970-02-14', 'Bolivia'),
		(3, 'Jorge', 'Gonzales', 'M', '1995-10-21', 'Bolivia'),
		(4, 'Sergio', 'Frederik',	'M', '1953-03-16', 'Brasil'),
		(5, 'Roberto', 'Mendez',	'M', '1969-07-24', 'Colombia'),
		(6, 'Danitza', 'Troncozo',	'F', '1950-08-11', 'Peru'),
		(7, 'Maira', 'Pinto',	'F', '1970-09-09', 'Argentina');