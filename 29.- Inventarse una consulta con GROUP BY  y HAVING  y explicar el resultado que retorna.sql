USE Practica2 ;
--Retorna los libros listados por precio, con precio mayor a 20
SELECT precio
FROM Libro
GROUP BY precio
HAVING precio > 20