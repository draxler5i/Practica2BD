USE Practica2 ;

SELECT Persona.genero, COUNT(*) as Cantidad 
FROM dbo.Persona 
GROUP BY Persona.genero
HAVING COUNT(Persona.persona_id) > 5;