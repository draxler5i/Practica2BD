USE Practica2 ;

INSERT INTO Libro(libro_id, titulo, precio, persona_id)
VALUES	(1, 'Emprendiendo aprendiendo', 85.14, 1),
		(2, '13 reasons why', 45.14, 2),
		(3, 'La lucha sin fin', 55.15, 3),
		(4, 'Mi guerra', 65.12, 4),
		(5, 'melancolia', 75.15, 5),
		(6, 'los angeles', 85.15, 1),
		(7, 'El principito', 130.00, 5);